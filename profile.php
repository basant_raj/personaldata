<?php
require 'connection.php';
session_start();
if (!isset($_SESSION['logged_in'])) {
	$_SESSION['error']="Please Login First";
	header('location: login.php');
}
?>
<!DOCTYPE html>
<html>
<head>
	<title>Profile Page</title>
	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">

	<link rel="stylesheet" type="text/css" href="css/mycss.css">
</head>
<body>

<div class="container-fluid">
	<div class="row" style="background-color: green;">
			<div class="col-md-8"><h1 style="color: cyan;">Welcome To BioBook</h1></div>
			<div class="col-1"><a href="home.php" style="text-decoration: none; color: orange;">Home</a></div>
			<div class="col-1"><a href="signup.php" style="text-decoration: none; color: orange;">Signup</a></div>
			<div class="col-1"><a href="login.php" style="text-decoration: none; color: orange;">Login</a></div>
			<div class="col-1"><a href="logout.php" style="text-decoration: none; color: orange;">Logout</a></div>
		</div>
		<div class="row">
		 <div class="col-md-12">
	       <form action="" method="post" class="form">
	       	<div class="form-group row">
	       		<div class="col-sm-4"></div>
	       		<label class="col-sm-1" style="color:green; font-size: 30px;">Email</label>
	       		<div class="col-sm-5"><input type="email" name="email" class="form-control"></div>
	       		<div class="col-sm-2"><button type="submit" class="btn btn-success">Search</button>
	       		</div>
	       	</div>
		</form>
		<?php if(isset($_POST) && isset($_SESSION['error'])) {
			echo $_SESSION['error'];
			unset($_SESSION['error']);
		}
		?>
	</div>
	</div>
	<?php
if (isset($_POST) && !empty($_POST)) {
$sql="SELECT * FROM users WHERE Email='".$_POST['email']."'"; 
$query=mysqli_query($conn,$sql);
if (!$query) {
	echo "Query failed";
	exit;
}
$row=mysqli_num_rows($query);
if ($row<=0) {
	$_SESSION['error']="<h1>No Members are registered with this email</h1>";
	header('location: profile.php');
}else{
	$data=mysqli_fetch_assoc($query);
}
?>
	<div class="row c2">
		<div class="col-sm-3">Name</div>
		<div class="col-sm-9"><?php echo $data['Name'];?></div>
	</div>
	<div class="row c2">
		<div class="col-sm-3">Address</div>
		<div class="col-sm-9"><?php echo $data['Address'];?></div>
	</div>
<div class="row c2">
		<div class="col-sm-3">Email Address</div>
		<div class="col-sm-9"><?php echo $data['Email'];?></div>
	</div>
	<div class="row c2">
		<div class="col-sm-3">Occupation</div>
		<div class="col-sm-9"><?php echo $data['Occupation'];?></div>
	</div>
	<div class="row c2">
		<div class="col-sm-3">Company Name</div>
		<div class="col-sm-9"><?php echo $data['Company'];?></div>
	</div>
	<div class="row c2">
		<div class="col-sm-3">Qualification</div>
		<div class="col-sm-9"><?php echo $data['Qualification'];?></div>
	</div>
	<div class="row c2">
		<div class="col-sm-3">Date Of Birth</div>
		<div class="col-sm-9"><?php echo $data['DOBirth'];?></div>
	</div>
	<div class="row c2">
		<div class="col-sm-3">Blood Group</div>
		<div class="col-sm-9"><?php echo $data['BGroup'];?></div>
	</div>
<?php }?>
</div>
</body>
</html>