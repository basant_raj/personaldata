<!DOCTYPE html>
<html>
<head>
	<title>Home Page</title>
	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="css/mycss.css">
</head>
<body class="c1">
	<div class="container-fluid">
		<div class="row" style="background-color: green;">
			<div class="col-8"><h1 style="color: cyan;">Welcome To BioBook</h1></div>
			<div class="col-1"><a href="home.php" style="text-decoration: none; color: orange;">Home</a></div>
			<div class="col-1"><a href="signup.php" style="text-decoration: none; color: orange;">Signup</a></div>
			<div class="col-1"><a href="login.php" style="text-decoration: none; color: orange;">Login</a></div>
			<div class="col-1"><a href="logout.php" style="text-decoration: none; color: orange;">Logout</a></div>
		</div>
	</div>
</body>
</html>