<?php 
require 'connection.php';
session_start();
if (isset($_SESSION['logged_in']) && $_SESSION['logged_in']==true) {
	header('location: profile.php');
}
 ?>
<!DOCTYPE html>
<html>
<head>
	<title>Login page</title>
	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
</head>
<body>
	<div class="container-fluid">
		<div class="row" style="background-color: green;">
			<div class="col-md-8"><h1 style="color: cyan;">Welcome To BioBook</h1></div>
			<div class="col-1"><a href="home.php" style="text-decoration: none; color: orange;">Home</a></div>
			<div class="col-1"><a href="signup.php" style="text-decoration: none; color: orange;">Signup</a></div>
			<div class="col-1"><a href="login.php" style="text-decoration: none; color: orange;">Login</a></div>
			<div class="col-1"><a href="logout.php" style="text-decoration: none; color: orange;">Logout</a></div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div>
					<?php
					   if (isset($_SESSION['error']) && !empty($_SESSION['error'])) {
					   	echo $_SESSION['error'];
					   	unset($_SESSION['error']);
					   }
					?>
				</div>
				<form action="loginprocessing.php" method="post" class="form">
					<div class="form-group row">
						<label class="col-sm-3">Email</label>
						<div class="col-sm-9"><input type="email" name="email" class="form-control">
					    </div> 
					</div>
					<div class="form-group row">
						<label class="col-sm-3">Password</label>
						<div class="col-sm-9"><input type="password" name="password" class="form-control"></div>
					</div>
					<div class="form-group row">
					    <label class="col-sm-3"></label>
					    <div class="col-sm-9">
					    <button type="submit" class="btn btn-success">Login</button>
					</div>
					</div>
				</form>
				
			</div>
		</div>
	</div>

</body>
</html>